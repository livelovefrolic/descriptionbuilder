# Description Builder
This tool is a Command line tool is used to automate the process of getting a product campaign automated.

## Features
The hope is to have all the following automated

[x] Generate a description based on a template file and a properties file

[] Generate a response email based on a template file and a properties file.

[] Rename all the add images to have keywords based on the properties file.

[] Generate a report file to specify what has been done in each file

[] Upload the description and ad photos to the seller account based on the properties and SKUs

[] Make the cli pipeline fully configurable.

## How to use 

TODO

## Author(s)
The author(s) of this tool are active seller and support this project in their free time. You are welcome to fork and use this product.
The author(s) ask that if you use this tool that proper credit is given to them. You can use this tool for your own commercial endeavors, but selling or reselling this tool is strict prohibited with out written permission from the author (Ryan Van Fleet).
 
 If you want to contribute please contact the author at rvanfleet.dev@gmail.com.
 
 
