package com.livelovefrolic.operations.description;

import com.google.common.collect.ImmutableMap;
import com.livelovefrolic.CLIConfig;
import com.livelovefrolic.ScanConfig;
import com.livelovefrolic.campaign.CampaignContext;
import com.livelovefrolic.operations.DescriptionOperation;
import com.livelovefrolic.operations.OperationException;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;
import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertTrue;

/**
 * Created by ry on 1/8/17.
 */
public class DescriptionOperationTest {

    final String testClasses = "target/test-classes";
    DescriptionOperation operation;

    @BeforeClass
    public void  beforeClass() throws IOException {
        File testDirectory = new File(getTestDirectory(null));
        File templateTestDir = new File(FilenameUtils.concat(testDirectory.getPath(), "templates"));
        File campaignTestDir = new File(FilenameUtils.concat(testDirectory.getPath(), "campaign"));

        FileUtils.deleteDirectory(testDirectory);
        FileUtils.forceMkdir(testDirectory);
        FileUtils.forceMkdir(campaignTestDir);
        FileUtils.forceMkdir(templateTestDir);
        FileUtils.copyFile(new File(FilenameUtils.concat(testClasses, "templates/description1-template.txt")),
                new File(FilenameUtils.concat(templateTestDir.getPath(), "description1-template.txt")));
        FileUtils.copyFile(new File(FilenameUtils.concat(testClasses, "templates/description2.txt")),
                new File(FilenameUtils.concat(templateTestDir.getPath(), "description2.txt")));
        FileUtils.copyFile(new File(FilenameUtils.concat(testClasses, "properties/properties1.txt")),
                new File(FilenameUtils.concat(campaignTestDir.getPath(), "properties.txt")));

    }

    @BeforeMethod
    public void beforeMethod() throws Exception{
        CommandLine cl = new DefaultParser().parse(CLIConfig.getOptions(),
                new String[]{getTestDirectory("templates"), getTestDirectory("campaign")});
        ScanConfig sc = new ScanConfig(cl);
        CampaignContext cc = new CampaignContext(new File(getTestDirectory("campaign")), sc);
        operation = new DescriptionOperation(cc);
    }

    @Test
    public void testRun() throws OperationException, IOException {

        operation.run();

        // Assertations
    }

    public String getTestDirectory(String directory){
        StringBuilder sb =  new StringBuilder("target/test/").append(this.getClass().getCanonicalName());
        if(StringUtils.isNotBlank(directory)){
            sb.append(File.separator). append(directory);
        }
        return sb.toString();
    }
}
