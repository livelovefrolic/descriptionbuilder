package com.livelovefrolic;

import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.ParseException;
import org.testng.annotations.Test;

import java.util.Arrays;

import static org.testng.AssertJUnit.assertEquals;

/**
 * Created by ry on 12/17/16.
 */
public class ScanConfigTest {

    @Test
    public void testConstructorTwoArgs() throws ParseException {
        ScanConfig conf = new ScanConfig(new DefaultParser().parse(CLIConfig.getOptions(), new String[]{"A","B"}));
        assertEquals("Template File", "A", conf.getTemplateDirectory().toString());
        assertEquals("Template File", "B", conf.getScanDirectory().getName());
        assertEquals("property filename", ScanConfig.DEFAULT_PROPERTIES_FILENAME, conf.getPropertyFilename());
    }

    @Test
    public void testConstructorThreeArgs() throws ParseException{
        ScanConfig conf = new ScanConfig(new DefaultParser().parse(CLIConfig.getOptions(), new String[]{"-p", "C", "A","B",}));
        assertEquals("Template File", "A", conf.getTemplateDirectory().toString());
        assertEquals("Template File", "B", conf.getScanDirectory().getName());
        assertEquals("property filename", "C", conf.getPropertyFilename());
    }

    @Test
    public void testConstructorFourArgs() throws ParseException{
        ScanConfig conf = new ScanConfig(new DefaultParser().parse(CLIConfig.getOptions(), new String[]{"-p", "C", "A","B"}));
        assertEquals("Template File", "A", conf.getTemplateDirectory().toString());
        assertEquals("Template File", "B", conf.getScanDirectory().getName());
        assertEquals("property filename", "C", conf.getPropertyFilename());
    }
}
