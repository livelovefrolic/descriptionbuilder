package com.livelovefrolic;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.testng.annotations.Test;

import java.util.List;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertTrue;

/**
 * Created by ry on 1/8/17.
 */
public class CLIConfigTest {


    @Test
    public void testParse() throws Exception{

        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = parser.parse(CLIConfig.getOptions(), new String[]{"-p", "props.txt", "abba", "Otherwise"});
        List<String> args = cmd.getArgList();
        assertEquals("First param", "abba", args.get(0));
        assertEquals("Second param", "Otherwise", args.get(1));
        assertTrue("Has p", cmd.hasOption('p'));
        assertFalse("Does not have help", cmd.hasOption('h'));
    }
}
