package com.livelovefrolic.campaign;

import com.livelovefrolic.ScanConfig;
import com.livelovefrolic.campaign.exceptions.CampaignParseException;
import com.livelovefrolic.campaign.exceptions.NotACampaignException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.rmi.runtime.Log;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * Created by ry on 1/8/17.
 */
public class CampaignContext {

    private final static Logger LOG = LoggerFactory.getLogger(CampaignContext.class);

    private final File campaignDirectory;
    private final ScanConfig scanConfig;
    private final Map<String, String> properties = Collections.synchronizedMap(new HashMap<>());
    private Set<String> campaignLogFile;

    /**
     * CampaignContext constructor
     * @param campaignDirectory
     * @param scanConfig
     */
    public CampaignContext(File campaignDirectory, ScanConfig scanConfig) throws NotACampaignException, CampaignParseException {
        this.campaignDirectory = campaignDirectory;
        this.scanConfig = scanConfig;
        initializeCampaignContext();
    }

    /**
     * Assumes the Campaign Directory and scan config have been set to the object.
     * steps
     * 1. First check to see if there is a properties file. If there is no properties file then this directory is not a campaign
     * 2. Parse the properties file
     * 3. Find the Campaign Log file.
     */
    public final void initializeCampaignContext() throws NotACampaignException, CampaignParseException {
        LOG.debug("Initializing the CampaignContext for", campaignDirectory.getName());

        Set<String> filesInDirectory = new HashSet<>(Arrays.asList(campaignDirectory.list()));
        LOG.debug("\tChecking for a properties file");
        if (!filesInDirectory.contains(scanConfig.getPropertyFilename())) {
            throw new NotACampaignException(campaignDirectory.getName()
                    + " does not contain a properties file ({}). Skip processing" + scanConfig.getPropertyFilename());
        }
        // Process the properties file
        parseProperties(buildCampaignFilename(scanConfig.getPropertyFilename()));
        LOG.debug("\tChecking for a Log file");
        campaignLogFile = Collections.synchronizedSet(new HashSet<>());
        if (filesInDirectory.contains(scanConfig.getCampaignLogFilename())) {
            // Has log file parse
            try {
                LOG.debug("\t\tCampaign log File found");
                // Nested ifs are ugly consider refactoring in the future
                if (scanConfig.overwriteCampaignLog()) {
                    LOG.debug("\t\tUser requested to overwrite the campaign log. Deleting the campaign log.");
                    FileUtils.forceDelete(new File(FilenameUtils.concat(
                            campaignDirectory.getPath(), scanConfig.getCampaignLogFilename())));
                } else {
                    LOG.debug("\t\tParsing campaign log file.");
                    campaignLogFile = Collections.synchronizedSet(new HashSet<>(IOUtils.readLines(
                            new FileReader(buildCampaignFilename(scanConfig.getCampaignLogFilename())))));
                }
            } catch (IOException e) {
                throw new CampaignParseException(e);
            }
        } else {
            LOG.debug("\t\tNo campaign log file exists. Start with blank");
        }
    }

    private String buildCampaignFilename(String filename) {
        return FilenameUtils.concat(campaignDirectory.getAbsolutePath(), filename);
    }

    private void parseProperties(String filename) throws CampaignParseException {
        LOG.debug("\t\tParsing the properties file");
        try {
            IOUtils.readLines(new FileReader(filename)).forEach(line -> {
                String ln = (String) line;
                if(StringUtils.isBlank(ln)){
                    //Leave lambda
                    return;
                }
                String[] tokens =  ln.split("=");
                properties.put(getStringFromTokenOrBlankIfNull(tokens,0),
                        getStringFromTokenOrBlankIfNull(tokens, 1));
            });
        } catch (IOException e) {
            throw new CampaignParseException("Could not parse " + filename, e);
        }
    }

    private String getStringFromTokenOrBlankIfNull(String[] tokens, int index){
        if(tokens.length <= index){
            return "";
        }
        String tok = tokens[index];
        return (StringUtils.isBlank(tok) ? "" : tok);
    }

    public String getCampaignDirectoryPath() {
        return campaignDirectory.getAbsolutePath();
    }

    public ScanConfig getScanConfig() {
        return scanConfig;
    }

    public Set<String> getCampaignLogFile() {
        return campaignLogFile;
    }

    /**
     * Returns an ImmutableMap of properties
     * @return The properties Map
     */
    public Map<String, String> getProperties() {
        return Collections.unmodifiableMap(this.properties);
    }
}
