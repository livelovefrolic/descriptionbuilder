package com.livelovefrolic.campaign.exceptions;

/**
 * Created by ry on 1/8/17.
 */
public class NotACampaignException extends Exception {

    /**
     * Constructor
     * @param message the message
     */
    public NotACampaignException(String message) {
        super(message);
    }
}
