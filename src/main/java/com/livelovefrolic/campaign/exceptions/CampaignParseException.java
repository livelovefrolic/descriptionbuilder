package com.livelovefrolic.campaign.exceptions;

/**
 * Created by ry on 1/8/17.
 */
public class CampaignParseException extends Exception{

    public CampaignParseException() {
        super();
    }

    public CampaignParseException(String message) {
        super(message);
    }

    public CampaignParseException(String message, Throwable cause) {
        super(message, cause);
    }

    public CampaignParseException(Throwable cause) {
        super(cause);
    }
}
