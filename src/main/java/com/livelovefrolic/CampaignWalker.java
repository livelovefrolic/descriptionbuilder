package com.livelovefrolic;

import com.livelovefrolic.campaign.CampaignContext;
import com.livelovefrolic.campaign.exceptions.CampaignParseException;
import com.livelovefrolic.campaign.exceptions.NotACampaignException;
import com.livelovefrolic.operations.IOperation;
import com.livelovefrolic.operations.OperationException;
import com.livelovefrolic.operations.OperationFactory;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Class that iterates through all the directories in the route and sets up the various campaign routines
 *
 * Created by ry on 12/17/16.
 */
public class CampaignWalker {

    private final static Logger LOG = LoggerFactory.getLogger(CampaignWalker.class);
    private final ScanConfig config;

    public CampaignWalker(ScanConfig config) {
        this.config = config;
    }

    public void run(){
        LOG.info("Running the Campaign Walker");
        // Get all the directories in the campaign folder
        List<File> campaigns = Arrays.asList(config.getScanDirectory().listFiles(new FileFilter() {
            @Override
            public boolean accept(File file) {
                return file.isDirectory();
            }
        }));

        LOG.debug("{} Sub Directories where found", campaigns.size());

        for (File campaign : campaigns){
            LOG.debug("Creating campaign context for {}", campaign.toString());
            try {
                CampaignContext campaignContext = new CampaignContext(campaign, config);
                for (IOperation op :OperationFactory.getOperations(campaignContext)){
                    op.run();
                }
                LOG.info("\tWriting Campaign Log file.");
                FileUtils.writeLines(new File(FilenameUtils.concat(campaign.getAbsolutePath(), config.getCampaignLogFilename())), campaignContext.getCampaignLogFile());

            } catch (NotACampaignException  e) {
                LOG.info(e.getMessage());
                continue;
            }catch (OperationException | IOException | CampaignParseException e){
                LOG.warn("Operation failed. Skipping campaign " + campaign.toString(), e);
                continue;
            }
        }
    }
}
