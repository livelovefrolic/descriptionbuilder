package com.livelovefrolic;

import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

/**
 * Ths class contains all the Command line parser configurations
 *
 * Created by ry on 1/8/17.
 */
public final class CLIConfig {

    public final static String APP_NAME = "description-builder";
    public final static String HELP_HEADER = "Takes in 2 arguments (template file directory, the directory to scan) and automatically generates a product description.";
    public final static String HELP_FOOTER = "";

    /** The HELP Option*/
    public final static Option HELP_OPTION = Option.builder("h")
            .longOpt("help")
            .required(false)
            .desc("show the help message")
            .build();

    /** Optional name for the properties file */
    public final static Option PROPERTY_FILENAME = Option.builder("p")
            .longOpt("property-filename")
            .required(false)
            .hasArg(true)
            .desc("The name of the property file to search for when generating the description. Defaults to " + ScanConfig.DEFAULT_PROPERTIES_FILENAME)
            .build();

    public final static Option CAMPAIGN_LOG_FILENAME = Option.builder("c")
            .longOpt("campaign-filename")
            .hasArg(true)
            .required(false)
            .desc("Change the name of the campaign filename. The campaign file captures what processing has been done to the campaign. Defaults to " + ScanConfig.DEFAULT_CAMPAIGN_FILENAME)
            .build();

    public  final static Option OVERWITE_CAMPAIGN = Option.builder("o")
            .longOpt("overwrite-campaign")
            .required(false)
            .hasArg(false)
            .desc("Reruns all the campaign templates.")
            .build();

    public final static Options OPTIONS;

    static {
        Options opts = new Options();
        opts.addOption(HELP_OPTION);
        opts.addOption(PROPERTY_FILENAME);
        opts.addOption(CAMPAIGN_LOG_FILENAME);
        opts.addOption(OVERWITE_CAMPAIGN);
        OPTIONS = opts;
    }

    public static Options getOptions(){
        return OPTIONS;
    }


}
