package com.livelovefrolic.util;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.filter.Filter;
import ch.qos.logback.core.spi.FilterReply;

/**
 * See http://stackoverflow.com/questions/10734025/logback-two-appenders-multiple-loggers-different-levels
 * One minor change where we specify FilterReply.ACCEPT if the name matches and the level is valid.  The referenced
 * implementation used FilterReply.NEUTRAL.
 *
 * To use this, place in the configuration file before any ThresholdFilters
 */
public class ThresholdLoggerFilter extends Filter<ILoggingEvent> {
    /** The level at which to log. Ex. DEBUG */
    private Level level;

    /** The package on which to apply the logger */
    private String logger;

    /**
     * Determine whether to log the event
     * @param event The event to log
     * @return whether or not to log
     */
    @Override
    public FilterReply decide(final ILoggingEvent event) {
        if (!isStarted()) {
            return FilterReply.NEUTRAL;
        }

        if (!event.getLoggerName().startsWith(logger)) {
            return FilterReply.NEUTRAL;
        }

        if (event.getLevel().isGreaterOrEqual(level)) {
            return FilterReply.ACCEPT;
        }
        return FilterReply.DENY;
    }

    public void setLevel(final Level level) {
        this.level = level;
    }

    public void setLogger(final String logger) {
        this.logger = logger;
    }

    public String getLogger() {
        return logger;
    }

    /**
     * Start the logger
     */
    @Override
    public void start() {
        if (level != null && logger != null) {
            super.start();
        }
    }
}