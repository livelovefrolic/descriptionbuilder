package com.livelovefrolic;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.filefilter.FileFileFilter;
import org.apache.commons.lang3.StringUtils;

import java.io.*;
import java.util.Arrays;
import java.util.List;

import static com.livelovefrolic.CLIConfig.*;
/**
 * Created by ry on 12/17/16.
 */
public class ScanConfig {

    public final static String DEFAULT_PROPERTIES_FILENAME = "properties.txt";
    public final static String DEFAULT_DESCRIPTION_FILENAME = "description.txt";
    public final static String DEFAULT_CAMPAIGN_FILENAME = "campaign.out";

    private File templateDirectory;
    private File scanDirectory;
    private final CommandLine commandLine;
    private final List<String> arguments;

    public ScanConfig(CommandLine cmd) {
        commandLine = cmd;
        arguments = cmd.getArgList();
    }

    public List<File> getTemplateFiles(){
        File templateDirectory = getTemplateDirectory();
        return Arrays.asList(templateDirectory.listFiles());
    }

    public File getTemplateDirectory() {
        if(templateDirectory == null && isValid()){
            templateDirectory = new File(arguments.get(0));
        }
        return templateDirectory;
    }

    public boolean isValid(){
        return arguments.size() == 2;
    }

    public File getScanDirectory() {
        if(scanDirectory == null && isValid()){
            scanDirectory = new File(arguments.get(1));
        }
        return scanDirectory;
    }

    public String getPropertyFilename() {
        return (commandLine.hasOption(PROPERTY_FILENAME.getOpt()) ? commandLine.getOptionValue(PROPERTY_FILENAME.getOpt()) : DEFAULT_PROPERTIES_FILENAME);
    }

    public String getCampaignLogFilename() {
        return (commandLine.hasOption(CAMPAIGN_LOG_FILENAME.getOpt()) ? commandLine.getOptionValue(CAMPAIGN_LOG_FILENAME.getOpt()) : DEFAULT_CAMPAIGN_FILENAME);
    }

    public boolean overwriteCampaignLog(){
        return commandLine.hasOption(OVERWITE_CAMPAIGN.getOpt());
    }

    public boolean hasHelp(){
        return commandLine.hasOption(HELP_OPTION.getOpt());
    }
}
