package com.livelovefrolic;


import org.apache.commons.cli.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by ry on 12/17/16.
 */
public class Main {

    private final static Logger LOGGER = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {

        CommandLineParser parser = new DefaultParser();
        try {
            CommandLine cmd = parser.parse(CLIConfig.getOptions(), args);

            ScanConfig config = new ScanConfig(cmd);
            if(config.hasHelp()){
                LOGGER.debug("Displaying help");
                printHelp();
                System.exit(0);
            }
            if (!config.isValid()) {
                LOGGER.error("At least 2 arguments are required. 1: description template. 2: The directory to scan");
                printHelp();
                System.exit(1);
            }

            new CampaignWalker(config).run();

        } catch (ParseException e) {
            LOGGER.error("An unexpected error occured while parsing the command line arguments: {}.", args);
            LOGGER.error("Exiting");
            System.exit(2);
        }
    }

    private static void printHelp(){
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp(CLIConfig.APP_NAME, CLIConfig.HELP_HEADER, CLIConfig.getOptions(), CLIConfig.HELP_FOOTER, true);
    }
}
