package com.livelovefrolic.operations;

import com.livelovefrolic.campaign.CampaignContext;

import java.util.ArrayList;
import java.util.List;

/**
 * Based on the CampaignContext and the ScanConfig configure the operations to run onthe directory
 * Created by ry on 1/8/17.
 */
public final class OperationFactory {

    public static List<IOperation> getOperations(CampaignContext campaignContext){

        List<IOperation> ops = new ArrayList<>();
        ops.add(new DescriptionOperation(campaignContext));
        return ops;
    }

}
