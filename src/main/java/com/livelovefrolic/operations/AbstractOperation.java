package com.livelovefrolic.operations;

import com.livelovefrolic.campaign.CampaignContext;
import org.slf4j.Logger;

/**
 * Created by ry on 1/8/17.
 */
public abstract class AbstractOperation implements IOperation{


    protected final CampaignContext campaignContext;

    /**
     * Constructor
     * @param campaignContext The campaign constructor
     */
    public AbstractOperation(CampaignContext campaignContext) {
        this.campaignContext = campaignContext;
    }

    @Override
    public boolean hasOperationBeenRun() {
        return campaignContext.getCampaignLogFile().contains(getOperationIdentifier());
    }

    @Override
    public void run() throws OperationException {
        getLogger().info("\tRunning operation: {}" , getOperationIdentifier());
        if (hasOperationBeenRun()){
            getLogger().debug("\t\tOperation has already been run. Skipping");
            return;
        }
        // Not ran Run operation
        runOperation();
        //Assume the operation was a success
        campaignContext.getCampaignLogFile().add(getOperationIdentifier());
    }

    protected abstract void runOperation() throws OperationException;

    /**
     * Get the subclasses logger
     * @return The logger
     */
    protected abstract Logger getLogger();
}
