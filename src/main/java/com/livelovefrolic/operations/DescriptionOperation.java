package com.livelovefrolic.operations;

import com.livelovefrolic.campaign.CampaignContext;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

import static com.livelovefrolic.ScanConfig.DEFAULT_DESCRIPTION_FILENAME;

/**
 *
 * Created by ry on 12/17/16.
 */
public class DescriptionOperation extends AbstractOperation {

    private final static Logger LOG = LoggerFactory.getLogger(DescriptionOperation.class);

    public final static String LOG_IDENTIFIER = "TEMPLATE";

    /**
     * Constructor
     * @param campaignContext The campaign constructor
     */
    public DescriptionOperation(CampaignContext campaignContext) {
        super(campaignContext);
    }

    @Override
    public void runOperation() throws OperationException {


        for (File templateFile : campaignContext.getScanConfig().getTemplateFiles()) {
            String template = null;
            try {
                template = IOUtils.toString(new FileReader(templateFile));

                    for (Map.Entry<String, String> entry : campaignContext.getProperties().entrySet()) {
                        template = StringUtils.replace(template, prepareToken(entry.getKey()), entry.getValue());
                    }
                    LOG.info("\t\tWriting the description file.");
                    IOUtils.write(template, new FileOutputStream(
                            FilenameUtils.concat(campaignContext.getCampaignDirectoryPath(),
                                buildDescriptionFilename(templateFile.getName()))));

            } catch (IOException e) {
                LOG.warn("Failed to create template for {}.", templateFile.getAbsolutePath());
            }
        }
    }

    private final static Set<String> PREFIXS = Collections.unmodifiableSet(
            new HashSet<>(Arrays.asList(".", "-", "_")));
    /**
     * Builds the description file from the template file name
     *
     *
     * @param templateFilename The template filename
     * @return The file name default DEFAULT_DESCRIPTION_FILENAME
     */
    public String buildDescriptionFilename(String templateFilename){
        if(StringUtils.isBlank(templateFilename)){
            return DEFAULT_DESCRIPTION_FILENAME;
        }
        if(!StringUtils.contains(templateFilename, "template")){
            // The name does not specify template so return the file name as is
            return templateFilename;
        }

        // remove the template word
        String[] tokens = templateFilename.split("template");
        // Check the last charater of the first token for (., -, _)
        String tok1 = tokens[0];
        if(PREFIXS.contains(tok1.charAt(tok1.length() - 1)));{
           tok1 =  tok1.substring(0, tok1.length() - 1);
        }
        return tok1 + tokens[1];
    }

    @Override
    protected Logger getLogger() {
        return LOG;
    }

    private String prepareToken(String token) {
        return "@@@" + token + "@@@";
    }

    @Override
    public String getOperationIdentifier() {
        return LOG_IDENTIFIER;
    }
}
