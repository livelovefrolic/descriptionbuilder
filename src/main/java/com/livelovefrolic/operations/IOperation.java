package com.livelovefrolic.operations;

import java.util.List;

/**
 * Interface for all operations.
 *
 * Created by ry on 1/8/17.
 */
public interface IOperation {

    /**
     * Runs the main operation routine
     */
    public void run() throws OperationException;

    /**
     * Checks a list of string to see if this operation has been logged as run.
     * @return true if the operation has been run. false otherwise
     */
    public boolean hasOperationBeenRun();

    /**
     * Gets the identifier for the operations
     * @return the identifier
     */
    public String getOperationIdentifier();
}
